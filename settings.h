 #include <arduino.h>

//UART ports
#define UART_1_RX 0
#define UART_1_TX 1

#define UART_2_RX_pin 9
#define UART_2_TX_pin 10

//CAN ports

#define CAN_TX_pin 3
#define CAN_RX_pin 4

//BLUETOOTH ports

#define BT_state_in 15

//TEMPERATURE inputs

#define onewire_pin 6

//lights inputs
#define RUNNING_LIGHT_IN        2
#define DIPPED_HEADLIGHT_IN     5
#define FULL_BEAM_IN            17  
#define LEFT_INDICATOR_IN       16
#define RIGHT_INDICATOR_IN      11
#define WARMING_INDICATOR_IN    12
#define FUNCTION_1_IN           14
#define FUNCTION_2_IN           18

//sensor inputs
#define SPEED_SENSOR_IN             13
#define NUMBER_OF_MAGNES            1
#define CIRCUMREFERENCE_OF_A_CIRCLE 1000 //obw w mm

//lights outputs
#define RUNNING_LIGHT_OUT       19
#define DIPPED_HEADLIGHT_OUT    20
#define FULL_BEAM_OUT           21
#define LEFT_INDICATOR_OUT      22
#define RIGHT_INDICATOR_OUT     23
