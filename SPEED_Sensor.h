#ifndef SPEED_Sensor_H
#define SPEED_Sensor_H

#include "Arduino.h"


class SPEED_Sensor
{
  private:
  uint16_t  actual_pulse;
  uint8_t   last_pulse ;
  uint8_t   circumference_of_a_circle;
  uint8_t   number_of_magnets;
  uint16_t  tmp;
  uint16_t  actual_speed;
  public:
  SPEED_Sensor(int circumference_of_a_circle, int number_of_magnets);
  void  Actual_pulse();
  void  Actual_speed();
  int   speed_status();
  
}; 

#endif 
