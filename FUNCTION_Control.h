#ifndef FUNCTION_Control_H
#define FUNCTION_Control_H

#include "Arduino.h"


class FUNCTION_Control
{
  private:
  volatile bool f_status;
  int pin1;
  int function_delay = 15;
  public:
  FUNCTION_Control(int pin1);
  void function_toggle();
  void Imitial_start();
  bool function_status(); 
}; 

#endif 
