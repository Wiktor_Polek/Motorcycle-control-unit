    
#include "OneWire.h"
#include "DallasTemperature.h"
#include "Timers.h"
#include "LIGHT_Control.h"
#include "FUNCTION_Control.h"
#include "settings.h"
#include "parameters.h"
#include "SPEED_Sensor.h"
#include "TinyGPS.h"
#include "nextion.h"

Timers <5> maintimer;

//Function
void gps_data(TinyGPS &gps);


void setup() 
{
 Serial.begin(9600); //UART for USB connection
 Serial2.begin(9600); // UART for nextion display -19200 ok
 Serial3.begin(9600); //UART for GPS
 
 
 //declaration of inputs
 pinMode(RUNNING_LIGHT_IN, INPUT_PULLUP);
 pinMode(DIPPED_HEADLIGHT_IN, INPUT_PULLUP);
 pinMode(FULL_BEAM_IN, INPUT_PULLUP);
 pinMode(LEFT_INDICATOR_IN, INPUT_PULLUP);
 pinMode(RIGHT_INDICATOR_IN, INPUT_PULLUP);
 pinMode(WARMING_INDICATOR_IN, INPUT_PULLUP);
 pinMode(FUNCTION_1_IN, INPUT_PULLUP);
 pinMode(FUNCTION_2_IN, INPUT_PULLUP);
 pinMode(SPEED_SENSOR_IN, INPUT_PULLUP);


 //interrupt declaration
 attachInterrupt(digitalPinToInterrupt(RUNNING_LIGHT_IN), runninglight, CHANGE);
 attachInterrupt(digitalPinToInterrupt(DIPPED_HEADLIGHT_IN), deppedheadlight, CHANGE);
 attachInterrupt(digitalPinToInterrupt(FULL_BEAM_IN), fullbeam, CHANGE);
 attachInterrupt(digitalPinToInterrupt(LEFT_INDICATOR_IN), leftindicator, CHANGE);
 attachInterrupt(digitalPinToInterrupt(RIGHT_INDICATOR_IN),rightindicator, CHANGE);
 attachInterrupt(digitalPinToInterrupt(WARMING_INDICATOR_IN),emergencyindicator, CHANGE);
 attachInterrupt(digitalPinToInterrupt(FUNCTION_1_IN), function1, CHANGE);
 attachInterrupt(digitalPinToInterrupt(FUNCTION_2_IN), function2, CHANGE);
 attachInterrupt(digitalPinToInterrupt(SPEED_SENSOR_IN), impulsemeasurement, FALLING);

  //declaration of timers
 maintimer.attach(0, 1000, speedmeasurement);
 maintimer.attach(1, 0, leftblinkerpulse);
 maintimer.attach(2, 0, rightblinkerpulse);
 maintimer.attach(3, 0, emergency);
 maintimer.attach(4,START_ANIM_DELAY,Nextion_Loop);


//Sensors initialization
 sensors.begin(); //Initialization of temperature sensors
//End of sensors initialization
}

void loop() 
{  
  if(tmp_1 == false)
  {
    softstart();
    tmp_1=true;
  }
maintimer.process();
//Serial.print("Poczatek petli:");
//Serial.println(millis());  
maintimer.process();     //timer initialization

//Serial.print("Odczyt temperatury - początek:");
//Serial.println(millis());
  //Temperature reading
  sensors.requestTemperatures(); //Pobranie temperatury z czujnika
  temp_eng = sensors.getTempCByIndex(0);
//Serial.print("Odczyt temperatury - koniec:");
//Serial.println(millis());

 
  //Temperature display
  //Serial.print("Temperatura silnika: ");
  //Serial.println(temp_eng);  //Wyswietlenie informacji
   //End of temperature display


  //GPS reading
     while (Serial3.available()) {

//Serial.print("Odczyt danych z gps - początek:");
//Serial.println(millis());
 
      if(gps.encode(Serial3.read()))
      {
        gps_data(gps);
        newdata=true;
      }
//Serial.print("Odczyt danych z gps - koniec:");
//Serial.println(millis());
    }

 
  //GPS test
  if(newdata){
              Serial.print("Długosc:"); Serial.println(latitude,6);
              Serial.print("Szerokosc:"); Serial.println(longtitude,6);
              Serial.print("Preskosc:");Serial.println(speed);
              Serial.print("Data:"); Serial.print(day); Serial.print("/"); Serial.print(month); Serial.print("/");Serial.println(year);
              Serial.print("Czas:"); Serial.print(hour); Serial.print(":"); Serial.print(minute); Serial.print(":"); Serial.println(second);
              newdata = false;
              }
  
    /*    
      //inputs test
      Serial.print("start");
      Serial.println (Runnig_light.light_status());
      Serial.println (Dipped_headlights.light_status());
      Serial.println (High_beam.light_status());
      Serial.println (Left_indicator.light_status());
      Serial.println (Rights_indicator.light_status());
      Serial.println (Warming_indicator.light_status());
      Serial.println (Stop_light.light_status());
      Serial.println (millis());
      delay(100);

*/
    
     //speed test
     //Serial.println("actual speed");
     
    // Serial.println(Speedmeasurement.speed_status());
     
 } //end main


  void softstart()
  {
    Runnig_light.Imitial_start();
    Dipped_headlights.Imitial_start();
    High_beam.Imitial_start();
    Left_indicator.Imitial_start();
    Rights_indicator.Imitial_start();
    Warming_indicator.Imitial_start();
    Function_1.Imitial_start();
    Function_2.Imitial_start();
  }
  
  void impulsemeasurement ()//przerwanie gdy czujnik wykryje magnes
  {
     Speedmeasurement.Actual_pulse();
     
  }

  void speedmeasurement ()
  {
    
     Speedmeasurement.Actual_speed();//sumuje ilosc impulsow co dany okres i wylicza predkosc w km 
  }

void leftblinkerpulse()
  {
     Left_indicator.blinding();
  }

void rightblinkerpulse()
  {
     Rights_indicator.blinding();
  }

void emergency()
  {
     Left_indicator.blinding();
     Rights_indicator.blinding();
  }
  
void runninglight()//postojowe
{
  Runnig_light.light_toggle();
}

void deppedheadlight()// krotkie uruchomia sie jedynie gdy mamy postojowe
{
  Dipped_headlights.light_toggle_parameter(Runnig_light.light_status());
}

void fullbeam() //dlugie tylko w tedy zadzialaja gdy sa uruchomione krotkie 
{
  High_beam.light_toggle_parameter(Dipped_headlights.light_status());
}

void leftindicator()//lewy kieunke zalacz/wylacz timer
{
  if(Left_indicator.blinker() == true)
  {
    maintimer.updateInterval(1, 400);
  }
  else
  {
    maintimer.updateInterval(1, 0);
  }
}

void rightindicator() //prawy kieunke zalacz/wylacz timer
{
  if(Rights_indicator.blinker() == true)
  {
    maintimer.updateInterval(2, 400);
  }
  else
  {
    maintimer.updateInterval(2, 0);
  }
}

void emergencyindicator()
{
  if(Warming_indicator.blinker() == true)
  {
    maintimer.updateInterval(3, 400);
  }
  else
  {
    maintimer.updateInterval(3, 0);
  }
}

void function1()
{
  Function_1.function_toggle();
}

void function2()
{
  Function_2.function_toggle();
}

void gps_data(TinyGPS &gps)
{
  Serial.print(millis());
  unsigned long age;
  long latitude_long, longtitude_long, altitude_long;
  gps.get_position(&latitude_long, &longtitude_long, &age);
  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  speed = gps.f_speed_kmph();
  altitude_long = gps.altitude();

  hour = hour +1; //difference of time zone
  latitude = (float)latitude_long / 1000000; //conversion to float
  longtitude = (float)longtitude_long / 1000000; //conversion to float
  altitude = altitude_long / 100; //convesion to meter
}
