#ifndef LIGHT_Control_H
#define LIGHT_Control_H

#include "Arduino.h"


class LIGHT_Control
{
  private:
  volatile bool l_status;
  bool tmp;
  int pin1;
  int pin2;
  volatile bool status1;
  int light_delay = 15;
  public:
  LIGHT_Control(int pin1, int pin2 );
  void light_toggle();
  void Imitial_start();
  void light_toggle_parameter(bool status1) ;
  bool blinker();
  bool light_status(); 
  void blinding(); 
}; 

#endif 
