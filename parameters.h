#include <arduino.h>
#include <TinyGPS.h>
#include "LIGHT_Control.h"
#include "FUNCTION_Control.h"
#include "SPEED_Sensor.h"
#include "OneWire.h"
#include "DallasTemperature.h"
//DELAY

//TEMPORARY
volatile int tmp_1               =  0;
volatile bool tmp_2              =  false;
//INPUTS & OUTPUTS
//creating object
LIGHT_Control Runnig_light(RUNNING_LIGHT_IN,RUNNING_LIGHT_OUT);
LIGHT_Control Dipped_headlights(DIPPED_HEADLIGHT_IN,DIPPED_HEADLIGHT_OUT);
LIGHT_Control High_beam(FULL_BEAM_IN,FULL_BEAM_OUT);
LIGHT_Control Left_indicator(LEFT_INDICATOR_IN,LEFT_INDICATOR_OUT);
LIGHT_Control Rights_indicator(RIGHT_INDICATOR_IN,RIGHT_INDICATOR_OUT);
LIGHT_Control Warming_indicator(WARMING_INDICATOR_IN,LEFT_INDICATOR_OUT);
FUNCTION_Control Function_1(FUNCTION_1_IN);
FUNCTION_Control Function_2(FUNCTION_2_IN);
SPEED_Sensor  Speedmeasurement(CIRCUMREFERENCE_OF_A_CIRCLE,NUMBER_OF_MAGNES);


//SENSOR_TEMPEATURE - w przypadku problemu z pamiecia zmien te floaty
OneWire oneWire(onewire_pin); //declaration of pin for Onewire
DallasTemperature sensors(&oneWire); //Getting information to DallasTemperature library about the pin

volatile float temp_eng             =   0;
volatile float temperature_1             =   0;

//GPS_SENSOR
TinyGPS gps;
double latitude, longtitude, altitude, speed;
int  year,x;
byte month, day, hour, minute, second, hundredths; 
unsigned long age, date, time, chars;
unsigned short sentences, failed;
bool newdata = false;
