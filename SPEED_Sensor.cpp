#include "SPEED_Sensor.h"
 

SPEED_Sensor::SPEED_Sensor(int circumference_of_a_circle, int number_of_magnets)
{
    
    this->actual_pulse = 0;
    this->last_pulse   = 0;
    this->tmp          = 0;
    this->actual_speed = 0;
    this->circumference_of_a_circle = circumference_of_a_circle;
    this->number_of_magnets = number_of_magnets;
}

void SPEED_Sensor::Actual_pulse()
{
  actual_pulse += 1;
 // Serial.println(actual_pulse);
}

void SPEED_Sensor::Actual_speed()
{
  last_pulse    = actual_pulse;
  actual_pulse  = 0 ;
  tmp           = (last_pulse*circumference_of_a_circle)/(number_of_magnets*1000); //prędkosc w m/s
  actual_speed  = tmp*36/10;
  //Serial.println(actual_pulse);
  //Serial.println(last_pulse);
 // Serial.println(actual_speed);
}

int SPEED_Sensor::speed_status()
{
return actual_speed ;
}
